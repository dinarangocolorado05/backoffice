import Navbar from '../src/components/Navbar';
import './App.css';

function App() {
  return (
    <div className="App">
      <Navbar />
      <h1>Hello world</h1>
    </div>
  );
}

export default App;
